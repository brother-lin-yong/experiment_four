package com.example.experiment_four;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;




public class MyService extends Service {


    private int count=0;
    private boolean quit=false;
    private MyServiceBinder myServiceBinder=new MyServiceBinder();
    public MyService() {

    }

    @Override
    public IBinder onBind(Intent intent) {
        return myServiceBinder; }

    @Override
    public void onCreate() {
        super.onCreate();
        //创建新 Thread，如果 service 启动每秒钟 count++，如果 quit 为真则退出
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (!quit) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    count++;
                }
            }
        });
        thread.start();
    }

    class MyServiceBinder extends Binder {
        public int getCount(){
            return MyService.this.count; } }

    @Override
    public void onDestroy() {
        super.onDestroy();
        quit=true;
        System.out.println("Service is Destroyed");}
}